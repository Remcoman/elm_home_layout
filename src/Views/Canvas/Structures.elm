module Views.Canvas.Structures exposing (structuresLayer)

import Svg exposing (g, Svg, rect, text)
import Svg.Attributes exposing (x, y, width, height, fill)
import Svg.Events exposing (on)
import Types.Model exposing (Model, Msg(..), ID)
import Types.Point exposing (Point)
import Types.Structure exposing (Structure(..), WallData)
import Views.Canvas.Wall exposing (wall)
import Json.Decode as Json
import Tuple exposing (first, second)


pointDecoder : (Point -> msg) -> Json.Decoder msg
pointDecoder msg =
    Json.map2 (\x y -> ( x, y )) (Json.field "clientX" Json.float) (Json.field "clientY" Json.float)
        |> Json.andThen (\point -> Json.succeed (msg point))


onStartDrag : (Point -> msg) -> Svg.Attribute msg
onStartDrag msg =
    Svg.Events.on "mousedown" (pointDecoder msg)


maybeToView : (a -> Svg Msg) -> Maybe a -> Svg Msg
maybeToView tagger a =
    case a of
        Just v ->
            tagger v

        Nothing ->
            text ""


isSelected : Maybe ID -> ID -> Bool
isSelected selectedStructure id =
    selectedStructure |> Maybe.map (\n -> n == id) |> Maybe.withDefault False


mapToView : Maybe ID -> Structure -> Svg Msg
mapToView selectedStructure structure =
    let
        isSel =
            isSelected selectedStructure
    in
        case structure of
            Wall id wallData ->
                wall (isSel id) id wallData


sizeLayer : Model -> Svg Msg
sizeLayer model =
    rect
        [ fill "transparent"
        , width (first model.actualGridSize |> toString)
        , height (second model.actualGridSize |> toString)
        ]
        []


structuresLayer : Model -> Svg Msg
structuresLayer model =
    g [ onStartDrag StartDrawingAWall ]
        ([ sizeLayer model ]
            ++ List.map (mapToView model.selectedStructure) model.structures
            ++ [ maybeToView (wall False 0) model.newWall ]
        )
