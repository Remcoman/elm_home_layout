module Views.Canvas.Grid exposing (grid)

import Svg exposing (..)
import Svg.Attributes exposing (id, class, width, height, viewBox, x1, x2, y1, y2)
import Types.Model exposing (Model, Msg(..))
import Types.Unit exposing (toMm, Unit(..))
import Tuple exposing (first, second)


horizontalLine : Model -> Int -> List (Svg.Attribute msg)
horizontalLine model index =
    let
        floatIndex =
            toFloat index

        x =
            second model.res |> .value |> toMm |> (*) floatIndex |> toString
    in
        [ x1 x
        , y1 "0"
        , x2 x
        , y2 (second model.actualGridSize |> toString)
        ]


verticalLine : Model -> Int -> List (Svg.Attribute msg)
verticalLine model index =
    let
        floatIndex =
            toFloat index

        y =
            first model.res |> .value |> toMm |> (*) floatIndex |> toString
    in
        [ x1 "0"
        , y1 y
        , x2 (first model.actualGridSize |> toString)
        , y2 y
        ]


gridLine : (Int -> List (Svg.Attribute msg)) -> Int -> Svg msg
gridLine coordsMapper index =
    line (coordsMapper index) []


grid : Model -> Svg Msg
grid model =
    let
        horizontalLines =
            List.range 0 (first model.cells) |> List.map (gridLine (horizontalLine model))

        verticalLines =
            List.range 0 (second model.cells) |> List.map (gridLine (verticalLine model))
    in
        g [ Svg.Attributes.class "grid" ]
            [ g [ Svg.Attributes.class "grid__lines-h" ] horizontalLines
            , g [ Svg.Attributes.class "grid__lines-v" ] verticalLines
            ]
