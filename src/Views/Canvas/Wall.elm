module Views.Canvas.Wall exposing (wall)

import Svg exposing (Svg, rect, Attribute)
import Svg.Attributes exposing (x, y, width, height, fill, class)
import Svg.Events exposing (onClick)
import Types.Model exposing (Msg(..), ID)
import Types.Point exposing (Point)
import Types.Unit exposing (Dir(..), toMm, toPoint, getDir)
import Types.Structure exposing (Structure, WallData)
import Tuple exposing (first, second)


svgClassList : List ( String, Bool ) -> Attribute msg
svgClassList list =
    class
        (List.filterMap
            (\( n, v ) ->
                if v then
                    Just n
                else
                    Nothing
            )
            list
            |> String.join " "
        )


horizontalWall : Point -> Point -> Float -> List (Attribute Msg)
horizontalWall p1 p2 thickness =
    let
        ( x1, y1 ) =
            p1

        ( x2, y2 ) =
            p2

        startX =
            min x1 x2

        endX =
            max x1 x2
    in
        [ x (startX |> toString)
        , y (y1 |> toString)
        , width (abs endX - startX |> toString)
        , height (thickness |> toString)
        ]


verticalWall : Point -> Point -> Float -> List (Attribute Msg)
verticalWall p1 p2 thickness =
    let
        ( x1, y1 ) =
            p1

        ( x2, y2 ) =
            p2

        startY =
            min y1 y2

        endY =
            max y1 y2
    in
        [ x (x1 |> toString)
        , y (startY |> toString)
        , width (thickness |> toString)
        , height (abs endY - startY |> toString)
        ]


wall : Bool -> ID -> WallData -> Svg Msg
wall selected id wall =
    let
        wallStart =
            ( first wall.start |> toMm, second wall.start |> toMm )

        wallEnd =
            ( first wall.end |> toMm, second wall.end |> toMm )

        thickness =
            toMm wall.thickness

        dir =
            getDir wall.start wall.end

        positionAttributes =
            case dir of
                Horizontal ->
                    horizontalWall wallStart wallEnd thickness

                Vertical ->
                    verticalWall wallStart wallEnd thickness

        commonAttributes =
            [ svgClassList
                [ ( "wall", True )
                , ( "wall--selected", selected )
                ]
            , onClick (SelectStructure id)
            ]
    in
        rect (commonAttributes ++ positionAttributes) []
