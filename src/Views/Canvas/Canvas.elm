module Views.Canvas.Canvas exposing (canvas)

import Types.Model exposing (Model, Msg(..))
import Svg.Attributes exposing (class, id, width, height, viewBox)
import Html exposing (div)
import Html.Attributes exposing (class)
import Tuple exposing (first, second)
import Svg exposing (..)


canvas : Model -> List (Svg Msg) -> Svg Msg
canvas model elements =
    let
        box =
            "0 0 " ++ (first model.actualGridSize |> toString) ++ " " ++ (second model.actualGridSize |> toString)
    in
        div [ Html.Attributes.class "canvas" ]
            [ svg
                [ id "canvas"
                , width "100%"
                , height "100%"
                , viewBox box
                ]
                elements
            ]
