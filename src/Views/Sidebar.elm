module Views.Sidebar exposing (sidebar)

import Html exposing (aside, Html, input, legend, div, fieldset, label, text)
import Html.Events exposing (on)
import Json.Decode
import Html.Attributes exposing (class, value)
import Types.Model exposing (Model, Msg(..))
import Types.Unit exposing (Unit, parse, toStr)
import Tuple exposing (first, second)


decode : Json.Decode.Decoder (Result String Unit)
decode =
    Html.Events.targetValue
        |> Json.Decode.andThen (\n -> Json.Decode.succeed (parse n))


onChange : (Result String Unit -> msg) -> Html.Attribute msg
onChange tagger =
    on "change" (Json.Decode.map tagger decode)


sidebar : Model -> Html Msg
sidebar model =
    aside [ class "sidebar" ]
        [ fieldset [ class "sidebar__fieldset" ]
            [ legend [] [ text "Oppervlakte" ]
            , div [ class "sidebar__area" ]
                [ div []
                    [ label [] [ text "Breedte" ]
                    , input [ onChange ChangeGridWidth, value (first model.gridSize |> .value |> toStr) ] []
                    ]
                , div []
                    [ label [] [ text "Hoogte" ]
                    , input [ onChange ChangeGridHeight, value (second model.gridSize |> .value |> toStr) ] []
                    ]
                ]
            ]
        , fieldset [ class "sidebar__fieldset" ]
            [ legend [] [ text "Afrastering" ]
            , div [ class "sidebar__area" ]
                [ div []
                    [ label [] [ text "Breedte" ]
                    , input [ onChange ChangeHorRes, value (first model.res |> .value |> toStr) ] []
                    ]
                , div []
                    [ label [] [ text "Hoogte" ]
                    , input [ onChange ChangeVertRes, value (second model.res |> .value |> toStr) ] []
                    ]
                ]
            ]
        ]
