port module Main exposing (..)

import Html exposing (Html, text, aside, div, input)
import Html.Attributes exposing (src, class, value)
import Types.FieldWithErr exposing (FieldWithErr, withDefault)
import Types.Model exposing (Model, defaultModel, Msg(..), updateModel)
import Types.Structure exposing (Structure(..), WallData, startWallFromPos, updateWallEndPos, defaultWallData)
import Types.Unit exposing (dist, UnitPoint, Unit)
import Views.Sidebar exposing (sidebar)
import Views.Canvas.Grid exposing (grid)
import Views.Canvas.Canvas exposing (canvas)
import Views.Canvas.Structures exposing (structuresLayer)
import Tuple exposing (first, second, mapFirst, mapSecond)
import Mouse


port getPosInViewbox : ( String, ( Float, Float ) ) -> Cmd msg


port resultPosInViewbox : (( Float, Float ) -> msg) -> Sub msg



---- MODEL ----


init : ( Model, Cmd Msg )
init =
    ( defaultModel, Cmd.none )



---- UPDATE ----


x : ( FieldWithErr Unit, FieldWithErr Unit ) -> Result String Unit -> ( FieldWithErr Unit, FieldWithErr Unit )
x oldValue newValue =
    mapFirst (\old -> withDefault old.value newValue) oldValue


y : ( FieldWithErr Unit, FieldWithErr Unit ) -> Result String Unit -> ( FieldWithErr Unit, FieldWithErr Unit )
y oldValue newValue =
    mapSecond (\old -> withDefault old.value newValue) oldValue


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeGridWidth val ->
            ( { model | gridSize = x model.gridSize val } |> updateModel, Cmd.none )

        ChangeGridHeight val ->
            ( { model | gridSize = y model.gridSize val } |> updateModel, Cmd.none )

        ChangeHorRes val ->
            ( { model | res = x model.res val } |> updateModel, Cmd.none )

        ChangeVertRes val ->
            ( { model | res = y model.res val } |> updateModel, Cmd.none )

        StartDrawingAWall val ->
            ( { model
                | selectedStructure = Nothing
                , startedDragging = True
                , newWall = Nothing
              }
            , getPosInViewbox ( "canvas", val )
            )

        UpdateDrawingWall val ->
            ( model, getPosInViewbox ( "canvas", ( toFloat val.x, toFloat val.y ) ) )

        StopDrawingAWall _ ->
            case model.newWall of
                Just wallData ->
                    if dist (first wallData.start) (first wallData.end) > 0 || dist (second wallData.start) (second wallData.end) > 0 then
                        ( { model
                            | startedDragging = False
                            , id = model.id + 1
                            , structures = model.structures ++ [ Wall model.id wallData ]
                            , newWall = Nothing
                          }
                        , Cmd.none
                        )
                    else
                        ( { model | startedDragging = False, newWall = Nothing }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        GotDragPosFromJS point ->
            let
                updatedWall =
                    case model.newWall of
                        Nothing ->
                            startWallFromPos model.cellSize point

                        Just wall ->
                            updateWallEndPos wall model.cellSize point
            in
                ( { model | newWall = Just updatedWall }, Cmd.none )

        SelectStructure id ->
            ( { model | selectedStructure = Just id }, Cmd.none )



---- VIEW ----


view : Model -> Html Msg
view model =
    div [ Html.Attributes.class "app" ]
        [ sidebar model
        , canvas model
            [ grid model
            , structuresLayer model
            ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.startedDragging then
        Sub.batch
            [ resultPosInViewbox GotDragPosFromJS
            , Mouse.ups StopDrawingAWall
            , Mouse.moves UpdateDrawingWall
            ]
    else
        Sub.batch
            [ resultPosInViewbox GotDragPosFromJS
            ]



---- PROGRAM ----


main : Program Never Model Msg
main =
    Html.program
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
