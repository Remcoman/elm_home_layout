import './main.css';
import { Main } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';

const App = Main.embed(document.getElementById('root'));

App.ports.getPosInViewbox.subscribe(function ([id, [x,y]]) {
    const svg = document.getElementById(id);
    const pt = svg.createSVGPoint();

    pt.x = x;
    pt.y = y;

    const cursorpt =  pt.matrixTransform(svg.getScreenCTM().inverse());

    App.ports.resultPosInViewbox.send([cursorpt.x, cursorpt.y]);
})

registerServiceWorker();

