module Types.Point exposing (..)


type alias Point =
    ( Float, Float )
