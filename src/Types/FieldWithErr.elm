module Types.FieldWithErr exposing (..)


type alias FieldWithErr v =
    { value : v
    , err : Maybe String
    }



-- returns FieldWithErr with new value or old value with error


withDefault : value -> Result String value -> FieldWithErr value
withDefault originalValue newValue =
    case newValue of
        Ok value ->
            FieldWithErr value Nothing

        Err errorMsg ->
            FieldWithErr originalValue (Just errorMsg)
