module Types.Unit exposing (..)

import Types.Point exposing (Point)
import Regex


type Dir
    = Horizontal
    | Vertical


type alias UnitPoint =
    ( Unit, Unit )


getDir : UnitPoint -> UnitPoint -> Dir
getDir p1 p2 =
    let
        ( startX, startY ) =
            p1

        ( endX, endY ) =
            p2
    in
        if abs (toMm startX - toMm endX) > abs (toMm startY - toMm endY) then
            Horizontal
        else
            Vertical


strToUnit : (Float -> Unit) -> String -> Result String Unit
strToUnit u v =
    v |> String.toFloat |> Result.map u


dist : Unit -> Unit -> Float
dist unit1 unit2 =
    abs ((toMm unit1) - (toMm unit2))


parse : String -> Result String Unit
parse str =
    let
        val =
            Regex.find (Regex.AtMost 1) (Regex.regex "(cm|mm|m)$") str
                |> List.head
                |> Maybe.andThen (\n -> List.head n.submatches |> Maybe.withDefault Nothing)
                |> Maybe.andThen (\n -> Just ( n, String.dropRight (String.length n) str ))
    in
        case val of
            Just ( unit, val ) ->
                case unit of
                    "mm" ->
                        val |> strToUnit Mm

                    "cm" ->
                        val |> strToUnit Cm

                    "m" ->
                        val |> strToUnit M

                    _ ->
                        Err "Invalid unit"

            Nothing ->
                Err "Invalid value"


toPoint : UnitPoint -> Point
toPoint ( x, y ) =
    ( toMm x, toMm y )


fromPoint : Point -> UnitPoint
fromPoint ( x, y ) =
    ( Mm x, Mm y )


toMm : Unit -> Float
toMm unit =
    case unit of
        Mm val ->
            val

        Cm val ->
            val * 100.0

        M val ->
            val * 10000.0


toStr : Unit -> String
toStr unit =
    case unit of
        Mm val ->
            (toString val) ++ "mm"

        Cm val ->
            (toString val) ++ "cm"

        M val ->
            (toString val) ++ "m"


type Unit
    = Mm Float
    | Cm Float
    | M Float
