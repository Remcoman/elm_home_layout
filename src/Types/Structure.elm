module Types.Structure exposing (..)

import Types.Unit exposing (Unit(..), toMm, fromPoint)
import Types.Point exposing (Point)


type Structure
    = Wall Int WallData


type alias WallData =
    { thickness : Unit
    , start : ( Unit, Unit )
    , end : ( Unit, Unit )
    }


defaultWallData : WallData
defaultWallData =
    WallData (Cm 30) ( Mm 0, Mm 0 ) ( Mm 0, Mm 0 )


snapToGrid : Point -> Point -> Point
snapToGrid ( cellWidth, cellHeight ) ( x, y ) =
    let
        col =
            floor (x / cellWidth) |> toFloat

        row =
            floor (y / cellHeight) |> toFloat
    in
        ( col * cellWidth, row * cellHeight )


snapToGridAlign : Point -> Float -> Point -> Point
snapToGridAlign cellSize thickness point =
    let
        ( cellx, celly ) =
            snapToGrid cellSize point

        ( x, y ) =
            point

        ( w, h ) =
            cellSize

        colDelta =
            (x - cellx) / w

        rowDelta =
            (y - celly) / h
    in
        ( cellx + (w - thickness) * (round (colDelta) |> toFloat)
        , celly + (h - thickness) * (round (rowDelta) |> toFloat)
        )


startWallFromPos : Point -> Point -> WallData
startWallFromPos cellSize pos =
    let
        unitPoint =
            snapToGridAlign cellSize (toMm defaultWallData.thickness) pos |> fromPoint
    in
        { defaultWallData | start = unitPoint, end = unitPoint }


updateWallEndPos : WallData -> Point -> Point -> WallData
updateWallEndPos wallData cellSize pos =
    let
        unitPoint =
            snapToGrid cellSize pos |> fromPoint
    in
        { wallData | end = unitPoint }
