module Types.Model exposing (..)

import Types.Unit exposing (Unit(..), toMm)
import Types.FieldWithErr exposing (FieldWithErr)
import Types.Structure exposing (Structure, WallData)
import Types.Point exposing (Point)
import Tuple exposing (first, second)
import Mouse


type alias ID =
    Int


type alias Model =
    { gridSize : ( FieldWithErr Unit, FieldWithErr Unit )
    , res : ( FieldWithErr Unit, FieldWithErr Unit )
    , actualGridSize : Point
    , cells : ( Int, Int )
    , cellSize : Point
    , startedDragging : Bool
    , id : ID
    , newWall : Maybe WallData
    , selectedStructure : Maybe ID
    , structures : List Structure
    }


defaultModel : Model
defaultModel =
    { gridSize = ( FieldWithErr defaultGridWidth Nothing, FieldWithErr defaultGridHeight Nothing )
    , res = ( FieldWithErr defaultVertRes Nothing, FieldWithErr defaultVertRes Nothing )
    , actualGridSize = ( defaultGridWidth |> toMm, defaultGridHeight |> toMm )
    , cellSize = ( 0, 0 )
    , startedDragging = False
    , id = 0
    , newWall = Nothing
    , selectedStructure = Nothing
    , structures = []
    , cells = ( 0, 0 )
    }
        |> updateModel


defaultGridWidth : Unit
defaultGridWidth =
    M 20


defaultGridHeight : Unit
defaultGridHeight =
    M 20


defaultHorRes : Unit
defaultHorRes =
    Cm 60


defaultVertRes : Unit
defaultVertRes =
    Cm 60


updateModel : Model -> Model
updateModel newModel =
    let
        horResMm =
            first newModel.res |> .value |> toMm

        vertResMm =
            second newModel.res |> .value |> toMm

        cells =
            ( (first newModel.gridSize |> .value |> toMm) / horResMm |> ceiling
            , (second newModel.gridSize |> .value |> toMm) / vertResMm |> ceiling
            )

        actualGridSize =
            ( (first cells |> toFloat) * horResMm
            , (second cells |> toFloat) * vertResMm
            )

        cellSize =
            ( first actualGridSize / (first cells |> toFloat)
            , second actualGridSize / (second cells |> toFloat)
            )
    in
        { newModel
            | actualGridSize = actualGridSize
            , cells = cells
            , cellSize = cellSize
        }


type Msg
    = ChangeGridWidth (Result String Unit)
    | ChangeGridHeight (Result String Unit)
    | ChangeHorRes (Result String Unit)
    | ChangeVertRes (Result String Unit)
    | StartDrawingAWall Point
    | UpdateDrawingWall Mouse.Position
    | StopDrawingAWall Mouse.Position
    | SelectStructure ID
    | GotDragPosFromJS Point
